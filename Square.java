/**
 * This is the class for square and to override the Abstract class
 * 
 * @author jpalepally
 *
 */
public class Square extends AbstractRectangle {

	public double area;

	public Square(double area) {

		super(4.0);
		this.area = area;
		// TODO Auto-generated constructor stub
	}
	
	public double area() {
		return area * area;
	}

	@Override

	// method to return the perimeter of a circle
	public double diameter() {
		return 4 * area;
	}

}
