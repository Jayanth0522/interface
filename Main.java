/**
 * This is the main class 
 * @author jpalepally1
 *
 */
public class Main {
	public static void main(String[] args) {
		
		// Circle Area and diameter
		double radius = 9;
		circle c1 = new circle(radius);
		System.out.println("circle Area :" +  c1.area());
		System.out.println("circle Periemeter:"  +  c1.periemeter());
		System.out.println("circle Diameter:"    +   c1.radius);
		
		
		
		
		
		//create object of square class
		Square sqobj=new Square(3.0);
		System.out.println("area of a square"+sqobj.area);
		System.out.println("perimeter of a square"+sqobj.periemeter()+"\n");
		/*
		 * finding the area and perimeter of a rectangle 
		 */
		Rectangle recobj=new Rectangle(5.0,6.2);
		System.out.println("area of rectangle"+recobj.area());
		System.out.println("perimeter of a rectangle "+recobj.periemeter());

		
	}

}
