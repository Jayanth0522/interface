/**
 * This is the class for Rectangle and override from Abstract class
 * @author jpalepally1
 *
 */
public class Rectangle1 extends AbstractRectangle {
	public double length;
	public double width;

	// Constructor for Rectangle1
	public  Rectangle1 (double length,double width) {
		super(5.0);
		this.length =length;
		this.width  =width;	
	}
	//method to return the area
	public double area() {
		return  length*width;
	}
	@Override
	//method to return the perimeter 
	public double periemeter() {
		return 2*(length+width);
	}

}
