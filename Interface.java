/**
 * This is the Program to Perform the given task
 * 
 * @author jpalepally1
 *
 */

interface Shape { // Creating an interface
	double area(); // Creating the method for Area

	double periemeter(); // Creating the Method for Periemeter

	double diameter();
}

class circle implements Shape { // Implementing the properties of shape to circle

	double radius;

	public circle(double radius) {
		this.radius = radius;
	}

	@Override
	public double area() { // Calculate the area of the circle

		return Math.PI * radius * radius;
	}

	@Override
	public double periemeter() { // Calculate the periemeter of circle

		return 2 * Math.PI * radius;
	}

	@Override
	public double diameter() { // Calculate the Diameter
		return 2 * radius;

	}

}

class Rectangle implements Shape { // Implementing the properties of shape to rectangle

	double length;
	double breadth;

	public Rectangle(double length, double breadth) {
		this.length = length;
		this.breadth = breadth;

	}

	@Override
	public double area() { // Calculating the area of Rectangle

		return length * breadth;
	}

	@Override
	public double periemeter() {// Calculating the periemter of Rectangle

		return 2 * (length + breadth);
	}

	@Override
	public double diameter() {
		// TODO Auto-generated method stub
		return 0;
	}

}
